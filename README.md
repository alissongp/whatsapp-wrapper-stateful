# Whatsapp Wrapper Stateful #

### Environment ###

Necessário para funcionamento

    python3.6
    pip
    firefox         ~version 79.0
    geckodriver     ~version 0.26.0
    virtualenv

### Como executar ###

Em desenvolvimento recomenda-se usar virtualenv, criado com:

    virtualenv -p python3.6 .

E habilitado com

    source bin/activate

Para desabilitar basta rodar

    deactivate

Primeiro instale as dependências com

    pip install -r requirements.txt

Para rodar em desenvolvimento pode usar o próprio WSGI do Django e executar

    gunicorn -b 127.0.0.1:<PORT> -t 180 -w 1 --thread 9 -k eventlet app.wsgi:application

Para rodar nos moldes de produção é utilizado o gunicorn:

    gunicorn -b 0.0.0.0:<PORT> -t 180 -w 1 --thread 9 app.wsgi:application

### Configurações básicas ###

As configurações básicas se encontram no .env, o projeto ainda não utiliza Consul.
