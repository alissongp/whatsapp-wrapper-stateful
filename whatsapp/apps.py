from django.apps import AppConfig
from .service import connection

connection.init()

class WhatsappConfig(AppConfig):
    name = 'whatsapp'
