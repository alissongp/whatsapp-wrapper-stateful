import pika
import json
import logging

from django.conf import settings
from ..data.user import User
from ..data.sender import Sender
from .server_api import upload_profile, recover_profiles, delete_profile, recover_dft_message

log = logging.getLogger(__name__)

conn = {}

class Connection(object):
    """
    Main class that holds all sessions for each provider
    """

    def __init__(self):
        self.whatsapps = {}
        self._parameters = pika.ConnectionParameters(host=settings.RABBITMQ_HOST,
                                                     port=settings.RABBITMQ_PORT,
                                                     credentials=pika.PlainCredentials(settings.RABBITMQ_USER, settings.RABBITMQ_PASS),
                                                     heartbeat=settings.RABBITMQ_HEARTBEAT)

        self.sender = Sender(self._parameters)
        self._start()

    def _start(self):
        """
        Instanciate all provider users
        """
        for provider in settings.PROVIDERS:
            self.whatsapps[provider] = User(provider, self._parameters)

        self._recover()

    def _recover(self):
        """
        Retrieve sessions if possible
        """
        profiles = recover_profiles()

        for prov in profiles:
            self.whatsapps[prov].whatsapp.recover_it()

    def get_code(self, provider):
        """
        Get respective QR Code for provider
        """
        return self.whatsapps[provider].whatsapp.get_code()

    def verify(self, provider):
        """
        Verify respective whatsapp connection for provider
        """
        return self.whatsapps[provider].whatsapp.verify()

    def publish(self, provider, message):
        """
        Pub a message into provider queue
        :param provider: associate to provider queue
        :param message: message to be sent
        """
        self.sender.publish(provider, message)

    def get_status(self, provider):
        """
        Get respective Driver status for provider
        """
        return self.whatsapps[provider].whatsapp.get_status()

    def drop(self, provider):
        """
        Drop respective Driver for provider
        """
        return self.whatsapps[provider].drop()

def init():
    conn['app'] = Connection()

def get_conn():
    return conn['app']
