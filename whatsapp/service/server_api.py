import io
import os
import shutil
import base64
import logging
import requests
import contextlib
from zipfile import ZipFile
from gql import gql, Client
from gql.transport.requests import RequestsHTTPTransport
from django.conf import settings
from django.core.cache import cache
from pathlib import Path

log = logging.getLogger(__name__)

def _login():
    """
    GraphQL connection to get access_token login, *cacheable*
    """

    if cache.get('TOKEN') is not None:
        return cache.get('TOKEN')

    try:
        _transport = RequestsHTTPTransport(
            url=f'{settings.API_HOST}:{settings.API_PORT}/graphql', verify=True, retries=3
        )
        client = Client(transport=_transport, fetch_schema_from_transport=True)

        query = gql(
            """
            query login ($username: String!, $password: String!) {
                login(username: $username, password: $password) {
                    body {
                        accessToken
                    }
                }
            }
            """
        )
        
        params = {"username": settings.API_USER, "password": settings.API_PASS}

        result = client.execute(query, variable_values=params)
        cache.set('TOKEN', result['login']['body']['accessToken'])
    except:
        log.exception('Error on connect to API')

    return cache.get('TOKEN')

def recover_dft_message(mnemonic):
    try:
        _transport = RequestsHTTPTransport(
            url=f'{settings.API_HOST}:{settings.API_PORT}/graphql',
            verify=True,
            headers = {
                'Authorization': f'Bearer {_login()}'
            }
        )
        client = Client(transport=_transport, fetch_schema_from_transport=True)

        query = gql(
            """
            query {
                findTemplate {
                    defaultMessage
                }
            }
            """
        )

        return client.execute(query)['findTemplate']['defaultMessage']
    except:
        log.exception('Error on connect API to recover')

    return None

def upload_profile(mnemonic, phone, profile_path):
    """
    Zip the path folder and upload on server api
    """

    zipped_path = f'{profile_path}/../wws-prof'
    shutil.make_archive(zipped_path, 'zip', profile_path)
    zipped_path += '.zip'

    req_headers = {
        "Authorization": f'Bearer {_login()}'
    }

    files = {'file': open(zipped_path, 'rb')}
    response = requests.post(f'{settings.API_HOST}:{settings.API_PORT}/api/profile/upload/{mnemonic}/{phone}', files=files, headers=req_headers)

    with contextlib.suppress(FileNotFoundError):
        os.remove('/tmp/profiles/wws-prof.zip')

def delete_profile(mnemonic):
    """
    Delete profile session on database
    """
    req_headers = {
        "Authorization": f'Bearer {_login()}'
    }

    response = requests.post(f'{settings.API_HOST}:{settings.API_PORT}/api/profile/delete/{mnemonic}', headers=req_headers)

def recover_profiles():
    """
    Recover wpp sessions on API and unzip on profile directory
    """
    req_headers = {
        "Authorization": f'Bearer {_login()}'
    }

    r = requests.get(f'{settings.API_HOST}:{settings.API_PORT}/api/profile/recover', headers=req_headers)
    profiles = []

    if r.status_code != 200:
        log.error(f'Error on recover provider profiles with status {r.status_code}')
    else:
        Path(f'/tmp/profiles').mkdir(parents=True, exist_ok=True)

        for profile in r.json():
            profiles.append(profile['mnemonic'])

            data = profile['whatsappProfile']['data']
            data = base64.decodebytes(bytes(data, 'utf-8'))

            with ZipFile(io.BytesIO(data), 'r') as filename:
                filename.extractall(path=f'/tmp/profiles/{profile["mnemonic"]}')

    return profiles
