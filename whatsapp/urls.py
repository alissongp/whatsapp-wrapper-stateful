from django.urls import path
from . import views

urlpatterns = [
    path('get_code', views.get_code, name='get_code'),
    path('verify', views.verify, name='verify'),
    path('send', views.send, name='send'),
    path('status', views.get_status, name='get_status'),
    path('drop', views.drop_connection, name='drop_connection'),
]
