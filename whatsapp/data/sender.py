import logging
from django.conf import settings
from json import dumps
from pika import BlockingConnection

log = logging.getLogger(__name__)

class Sender(object):
    """
    Class sender publish messages in respective provider queues,
    with reconnectable RabbitMQ connection when needed
    """

    def __init__(self, _params):
        self._params = _params
        self.chan = None
        self._conn = None
        self._connect()

    def _connect(self):
        """
        Connect RabbitMQ in fact, only sender
        """
        if self._conn is not None and self._conn.is_open:
            self._conn.close()

        self._conn = BlockingConnection(self._params)
        self.chan = self._conn.channel()
        self.chan.confirm_delivery()

    def publish(self, provider, message, retry=False):
        """
        Publish message into respective queue
        :param provider: provider in this scenario
        :param message: message to be sent
        :param retry: represents whether the connection will be reestablished
        """        
        if not self._conn.is_open or retry:
            self._connect()

        try:
            self.chan.basic_publish(exchange='', routing_key=f'{provider}-{settings.SUFFIX}', body=dumps(message))
        except:
            if not retry:
                log.warn('Trying to publish once another time')
                self.publish(provider, message, True)
            else:
                log.exception(f'Error on publish message to provider [{provider}]')
