import os
import logging

from django.conf import settings
from ..service.server_api import upload_profile
from .session import Session
from .whatsapp import Whatsapp

log = logging.getLogger(__name__)

class User(object):
    """
    User class, each provider has a user, and each user has a firefox and RabbitMQ connection
    """

    def __init__(self, provider, _params):
        self.provider = provider
        self.queue_name = f'{provider}-{settings.SUFFIX}'

        self.session = Session(self.queue_name, _params, self.send_message)
        self.whatsapp = Whatsapp(self.provider, self.session.consume, self.session.unconsume)

    def send_message(self, message):
        return self.whatsapp.send_message(message)

    def start_consume(self):
        """
        Start everything needed for User,
        both whatsapp & RabbitMQ
        """
        self.session.consume()

    def recover_all(self):
        """
        Recover everything needed for User,
        if recover fails, so close it
        """
        # stop RabbitMQ consume
        self.session.unconsume()
        # retrieve profile and try to establish whatsapp connection
        # on success or failure, the RabbitMQ session will already be handled
        self.whatsapp.recover_it()

    def drop(self):
        """
        Stop everything needed for User,
        both whatsapp & RabbitMQ
        """
        log.info(f'Forced dropping for {self.whatsapp}')
        self.whatsapp._on_fail()
