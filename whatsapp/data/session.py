import json
import time
import logging
import threading
from pika import SelectConnection
from pika.channel import Channel
from pika.spec import Basic, BasicProperties

log = logging.getLogger(__name__)

class Session(object):
    """
    RabbitMQ session, it always stays active, but the queue is only consumed if the phone is linked
    """

    def __init__(self, queue_name, _params, send_to_wpp):
        self._consuming = False
        self._params = _params
        self.send_message = send_to_wpp
        self.queue_name = queue_name

        self.chan = None
        self._closing = False
        self._conn = self.connect()
        self.th = threading.Thread(target=self._start)
        self.th.start()

    def connect(self):
        return SelectConnection(parameters=self._params, on_open_callback=self._on_connection_open)

    def _start(self):
        try:
            log.info(f'Provider {self.queue_name} was connected')
            self._conn.ioloop.start()
        except Exception:
            log.exception(f'Failed connecting consumer for {self.queue_name}')
        finally:
            log.warn(f'Stopped AMQP IOLoop for {self.queue_name}')

    def consume(self):
        if self._consuming:
            self.unconsume()

        log.info(f'Now starting to consume {self.queue_name}')
        self.chan.basic_consume(consumer_tag=self.queue_name, queue=self.queue_name, on_message_callback=self._handle_receive)
        self._consuming = True

    def unconsume(self):
        log.info(f'Restarting session of {self.queue_name}, it will no longer be consuming')
        if self.chan is not None:
            self.chan.basic_cancel(consumer_tag=self.queue_name)
            self.chan.close()

    def _on_connection_open(self, _connection: SelectConnection):
        self._conn.add_on_close_callback(self._on_connection_close)
        self._conn.channel(on_open_callback=self._on_channel_open)

    def _on_connection_close(self, _conn: SelectConnection, exception: Exception):
        self.chan = None
        self._consuming = False

        log.info(f'Closed consumer connection at {self.queue_name}, reopening in 5 seconds')
        time.sleep(5)
        self._reconnect()

    def _reconnect(self):
        self._conn.ioloop.stop()
        if not self._closing:
            self._conn = self.connect()
            self._conn.ioloop.start()
        else:
            self._closing = False
            self.th = threading.Thread(target=self._start)
            self.th.start()

    def _on_channel_open(self, channel: Channel):
        log.info(f'Rabbit consumer channel opened in queue: {self.queue_name}')
        try:
            self.chan = channel
            self.chan.add_on_close_callback(self._on_channel_close)
            self.chan.queue_declare(self.queue_name, durable=True)
        except Exception:
            log.exception('Failed setting channel')

    def _on_channel_close(self, reply_code, reply_text):
        log.warning('Channel was closed: (%s) %s', reply_code, reply_text)
        self._conn.close()

    def _handle_receive(self, _channel: Channel, method: Basic.Deliver, _header: BasicProperties, body):
        try:
            log.info(f'Received message {body} from {self.queue_name}')
            data = json.loads(body.decode("utf-8"))
            _can_ack = self.send_message(data)
            if _can_ack:
                log.info(f'Acking message {data}')
                self.chan.basic_ack(delivery_tag=method.delivery_tag)
            else:
                log.warn(f'Nacking message {data}')
                self.chan.basic_nack(delivery_tag=method.delivery_tag)
        except Exception:
            log.exception(f'Failed parsing body from {self.queue_name}')
