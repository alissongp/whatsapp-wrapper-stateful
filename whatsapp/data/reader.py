import time
import threading

class Reader(object):
    """
    Class to read new messages and send default message to them
    """

    def __init__(self, _message, _callback):
        self.dft_message = _message
        self.callback = _callback
        self.handling = False
        self.thread = threading.Thread(target=self._handle)

    def start(self, driver):
        """
        Start thread to really handle messages
        """
        self.stop()
        self.driver = driver
        self.handling = True
        self.thread.start()

    def stop(self):
        """
        Stop thread for now
        """
        self.driver = None
        if self.handling:
            self.handling = False
            self.thread.join()
            self.thread = threading.Thread(target=self._handle)

    def _handle(self):
        """
        Handle all new messages that arrive at phone
        """
        while self.handling:
            time.sleep(2)

            if not self.handling:
                break

            for contact in self.driver.get_unread():
                self.driver.chat_send_seen(contact.chat.id)
                msg_to_send = {
                    "phone": contact.chat.id,
                    "message": self.dft_message
                }

                self.callback(msg_to_send)
