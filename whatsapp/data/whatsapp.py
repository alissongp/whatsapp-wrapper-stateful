import os
import json
import shutil
import logging
from django.conf import settings
from webwhatsapi import WhatsAPIDriver, WhatsAPIDriverStatus
from ..service.server_api import upload_profile, delete_profile, recover_dft_message
from .reader import Reader
from apscheduler.schedulers.background import BackgroundScheduler
from django.core.cache import cache
from selenium.common.exceptions import InvalidSessionIdException

log = logging.getLogger(__name__)

class Whatsapp(object):
    """
    Whatsapp class, associates the user with whatsapp in fact, via gecko firefox connection
    """

    def __init__(self, _provider, _consume, _unconsume):
        self.provider = _provider
        self.consume = _consume
        self.unconsume = _unconsume
        self.phone = None
        self.connected = False
        self.profile_path = f'/tmp/profiles/{_provider}'
        self.driver = None
        self.reader = Reader(recover_dft_message(_provider), self.send_message)

        self.scheduler = BackgroundScheduler()
        self.scheduler.add_job(self._scheduled, 'interval', seconds=60)
        self.scheduler.add_job(self._recover_message, 'interval', minutes=settings.RECOVER_MESSAGE_DELAY)
        self.scheduler.start(paused=True)

    def _scheduled(self):
        try:
            if self.driver is None or self.driver.get_status() is not WhatsAPIDriverStatus.LoggedIn:
                cache.set(f'{self.provider}-STATUS', self.recover_it())
            else:
                cache.set(f'{self.provider}-STATUS', True)
        except InvalidSessionIdException:
            pass
        except:
            log.exception(f'Error on {self} scheduler')
            cache.set(f'{self.provider}-STATUS', self.recover_it())
        # log.info(cache.get(f'{self.provider}-STATUS'))

    def _recover_message(self):
        if self.reader is not None:
            self.reader.dft_message = recover_dft_message(self.provider)

    def _connect(self):
        if not os.path.exists(self.profile_path):
            os.makedirs(self.profile_path)

        self.driver = WhatsAPIDriver(
            loadstyles=False,
            client='firefox',
            profile=self.profile_path,
            headless=True
        )

    def recover_it(self):
        if not self._check_profile:
            self._on_fail()
            return False

        log.info(f'Recovering {self}')
        self.unconsume()
        self.scheduler.pause()
        self._close()
        self._connect()
        return self.verify()["whatsappStatus"]

    def get_code(self):
        if self.driver is None:
            log.info(f'{self} is not connected, connecting then')
            self._connect()

        if not self.connected:
            try:
                return self.driver.get_qr_base64()
            except:
                log.exception(f'Error on get {self} QR Code')

        return None

    def verify(self):
        if self.driver is None:
            return { "whatsappStatus": False }

        try:
            self.driver.wait_for_login(timeout=settings.TIMEOUT_CODE_EXPIRATION)
            self._on_success()

            return { "whatsappStatus": True, "whatsappPhone": self.phone }
        except:
            log.exception(f'Error on self connection to {self}')
            self._on_fail()
            return { "whatsappStatus": False }

    def get_status(self):
        """
        Get Driver status
        :return: true if connected, false otherwise
        """
        if cache.get(f'{self.provider}-STATUS') is None:
            return False
        return cache.get(f'{self.provider}-STATUS')

    def _save_profile(self):
        """
        Saves profile to disk & database
        """
        self.driver.save_firefox_profile()
        upload_profile(self.provider,  self.phone, self.profile_path)

    def _on_success(self):
        """
        Calls when driver is successfully connected
        """
        try:
            self.phone = self.driver.get_me()
            self._save_profile()
            self.connected = True
            self.consume()
            self.reader.start(self.driver)

            cache.set(f'{self.provider}-STATUS', True)
            self.scheduler.resume()

            log.info(f'Success on connecting {self}')
        except:
            log.exception('Unexpected Driver error on success')

    def _on_fail(self):
        """
        Calls when driver is unsuccessfully connected,
        can be called even if no driver is associated
        """
        try:
            self.phone = False
            self.connected = False
            self.reader.stop()
            self._delete_profile()
            self._close()
            self.unconsume()

            cache.set(f'{self.provider}-STATUS', False)
            self.scheduler.pause()
        except:
            log.exception('Unexpected Driver error on failure')

    def _delete_profile(self):
        try:
            if os.path.exists(self.profile_path):
                shutil.rmtree(self.profile_path)

            delete_profile(self.provider)
        except:
            log.exception(f'Error on remove path: {self.profile_path}')

    def _close(self):
        """
        Close firefox session
        """
        if self.driver is not None:
            try:
                self.driver.quit()
                log.info(f'Driver for {self} was successfully closed')
            except:
                log.exception(f'Error on close {self} driver')

        self.driver = None

    def _format_phone(self, phone):
        if not phone.startswith('55'):
            phone = '55' + phone
        if not phone.endswith('@c.us'):
            phone = phone + '@c.us'

        return phone

    def _format_nine(self, phone):
        phone = self._format_phone(phone)
        if len(phone.split("@")[0]) == 12:
            return phone[0:4] + "9" + phone[4:]
        elif len(phone.split("@")[0]) == 13:
            return phone[0:4] + phone[5:]
        return phone

    def _check_profile(self):
        """
        Check if profile is saved on /tmp and is connected
        """
        if os.path.exists(self.profile_path + "/localStorage.json") and os.path.exists(self.profile_path + "/user.js"):
            with open(self.profile_path + "/localStorage.json") as storage:
                if 'last-wid' in json.load(storage):
                    return True
        return False

    def _driver_needed(self):
        """
        Validate driver connection, to find out if need 
        reconnect/reestablish or just force unlink
        :return: true if needed, false otherwise.
        """
        validate = True
        # has no driver
        if self.driver is None:
            validate = False

        # has driver but not logged
        if validate and self.driver.get_status() is not WhatsAPIDriverStatus.LoggedIn:
            vaidate = False

        # there is no driver saved to reestablish
        if not validate and self._check_profile():
            log.info(f'Trying to reestablish Driver connection for {self}')
            validate = self.recover_it()
        elif not validate:
            self._on_fail()

        return validate

    def send_message(self, message):
        """
        Tries to send the message, if there is an
        error the driver closes and nack message
        """
        _is_ok = self._driver_needed()
        if _is_ok:
            self._send(message)

        return _is_ok

    def _send(self, message, retry=False):
        """
        Send message in fact, if message is not formatted as expected,
        or fail for another reason, message will be acked anyway
        """
        try:
            self.driver.create_if_not_exists(self._format_phone(message["phone"]))
            
            if "phone" not in message or "message" not in message:
                log.warn(f'The message did not come formatted as expected: {message}')
                return

            self.driver.chat_send_message(self._format_phone(message["phone"]), message["message"])
            log.info(f'Successfully on send {message}')
        except:
            if not retry:
                log.exception(f'Error on send first message [{message["phone"]}]')
                message["phone"] = self._format_nine(message["phone"])
                self._send(message, True)
            else:
                log.exception(f'Error on send message to [{message["phone"]}]')

    def __str__(self):
        if self.phone:
            return f'User[provider: [{self.provider}], connected: [{self.connected}], phone: [{self.phone}]]'
        else:
            return f'User[provider: [{self.provider}], connected: [{self.connected}]]'
