import json
import logging

from django.http import HttpResponse, HttpResponseNotFound, HttpResponseServerError, JsonResponse
from django.conf import settings
from .service import connection

log = logging.getLogger(__name__)

def _valid_provider(provider):
    """
    Validate arg provider
    :param provider: provider to validate
    :return: true if provider is configured, false otherwise
    """
    return provider in settings.PROVIDERS

def valid_headers(_headers):
    """
    Validates that the headers is as expected
    :param _headers: request headers
    :return: true if valid, false otherwise
    """
    if settings.PROVIDER_KEY not in _headers or not _valid_provider(_headers[settings.PROVIDER_KEY]):
        log.error("A provider has not been passed or it is not valid")
        return False
    return True

def get_code(request):
    """
    Return a base64 references for QR Code
    """
    if request.method == 'GET':
        _headers = request.headers
        if not valid_headers(_headers):
            return HttpResponseServerError()

        qr_code = connection.get_conn().get_code(_headers[settings.PROVIDER_KEY])

        return HttpResponse(json.dumps(qr_code), content_type='text/plain')
    else:
        return HttpResponseNotFound()

def verify(request):
    """
    Returns a object indicating whether the provider has the authenticated phone
    """
    if request.method == 'GET':
        _headers = request.headers
        if not valid_headers(_headers):
            return HttpResponseServerError()
    
        return JsonResponse(connection.get_conn().verify(_headers[settings.PROVIDER_KEY]))
    else:
        return HttpResponseNotFound()

def send(request):
    """
    Pub message to specifc rabbit queue
    """
    if request.method == 'POST':
        try:
            _headers = request.headers
            if not valid_headers(_headers):
                return HttpResponseServerError()

            body = request.body.decode("utf-8")
            body = json.loads(body)
            connection.get_conn().publish(_headers[settings.PROVIDER_KEY], body)

            return HttpResponse(status=200)
        except:
            log.exception("Server error during pub message")
            return HttpResponseServerError()
    else:
        return HttpResponseNotFound()

def get_status(request):
    """
    Return respective provider status
    """
    if request.method == 'GET':
        try:
            _headers = request.headers
            if not valid_headers(_headers):
                return HttpResponseServerError()

            return HttpResponse(json.dumps(connection.get_conn().get_status(_headers[settings.PROVIDER_KEY])), content_type='application/json')
        except:
            log.exception("Server error during get_status")
            return HttpResponseServerError()
    else:
        return HttpResponseNotFound()

def drop_connection(request):
    """
    Drop all connections established for respective provider
    """
    if request.method == 'POST':
        try:
            _headers = request.headers
            if not valid_headers(_headers):
                return HttpResponseServerError()

            connection.get_conn().drop(_headers[settings.PROVIDER_KEY])
            return HttpResponse(status=200)
        except:
            log.exception("Server error during drop_connection")
            return HttpResponseServerError()
    else:
        return HttpResponseNotFound()