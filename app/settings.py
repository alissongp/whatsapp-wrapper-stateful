import os

from dotenv import load_dotenv
from pathlib import Path

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

ENV_PATH = Path(BASE_DIR) / '.env'

load_dotenv(dotenv_path=ENV_PATH)

SECRET_KEY = os.getenv('SECRET_KEY')

DEBUG = os.getenv('DEBUG')

if not os.path.exists(os.getenv('LOG_PATH', '../logs')):
    os.mkdir(os.getenv('LOG_PATH', '../logs'))

ALLOWED_HOSTS = ['*']

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'whatsapp.apps.WhatsappConfig',
    'corsheaders'
]

CORS_ORIGIN_ALLOW_ALL = True
os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'corsheaders.middleware.CorsMiddleware',
]

ROOT_URLCONF = 'app.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': '/var/tmp/django_cache',
        'TIMEOUT': 300,
    }
}

WSGI_APPLICATION = 'app.wsgi.application'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            '()': 'whatsapp.logger.handler.TimezoneFormatter',
            'format': '[{asctime}] --- [{levelname}]: {message}',
            'datefmt': '%Y-%m-%d %H:%M:%S',
            'style': '{',
        }
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'default',
        },
        'file': {
            'class': 'whatsapp.logger.handler.EnhancedRotatingFileHandler',
            'filename': os.getenv('LOG_PATH', '../logs') + '/whatsapp-wrapper.log',
            'formatter': 'default',
            'when': 'midnight',
            'maxBytes': os.getenv("MAX_LOG_FILE_SIZE", '10g'),
            'backupCount': os.getenv("MAX_LOG_HISTORY", 30)
        }
    },
    'root': {
        'handlers': ['console', 'file'],
        'level': 'INFO',
    },
    'loggers': {
        'webwhatsapi': {
            'propagate': False,
        },
        'pika': {
            'propagate': False,
        },
        'urllib3': {
            'propagate': False,
        },
        'apscheduler': {
            'propagate': False,
        },
    }
}


LANGUAGE_CODE = 'en-us'

TIME_ZONE = os.getenv("TZ", "America/Sao_Paulo")

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'

# env properties

RABBITMQ_HOST = os.getenv('RABBITMQ_HOST')
RABBITMQ_PORT = os.getenv('RABBITMQ_PORT')
RABBITMQ_USER = os.getenv('RABBITMQ_USER')
RABBITMQ_PASS = os.getenv('RABBITMQ_PASS')
RABBITMQ_HEARTBEAT = int(os.getenv('RABBITMQ_HEARTBEAT'))

SUFFIX = os.getenv('SUFFIX')

USER_ALREADY_CONNECTED = os.getenv('USER_ALREADY_CONNECTED')

TIMEOUT_CODE_EXPIRATION = int(os.getenv('TIMEOUT_CODE_EXPIRATION'))

PROVIDER_KEY = os.getenv('PROVIDER_KEY')
WAREHOUSE_KEY = os.getenv('WAREHOUSE_KEY')

PROVIDERS = os.getenv('PROVIDERS').split(',')

API_HOST = os.getenv('API_HOST')
API_PORT = os.getenv('API_PORT')
API_USER = os.getenv('API_USER')
API_PASS = os.getenv('API_PASS')

RECOVER_MESSAGE_DELAY = int(os.getenv('RECOVER_MESSAGE_DELAY'))